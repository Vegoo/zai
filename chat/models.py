from django.db import models


class Role(models.Model):
    name = models.CharField(max_length=50)


class User(models.Model):
    mail = models.CharField(max_length=50)
    nickname = models.CharField(max_length=50)
    password = models.CharField(max_length=100)
    isActive = models.BooleanField(default=True)
    currentLogin = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    role = models.ForeignKey(Role, on_delete=models.DO_NOTHING)


class Friend(models.Model):
    User1_ID = models.ForeignKey(User, on_delete=models.CASCADE, related_name="User1")
    User2_ID = models.ForeignKey(User, on_delete=models.CASCADE, related_name="User2")


class Group(models.Model):
    name = models.CharField(max_length=50)
    isActive = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)


class MemberGroup(models.Model):
    Group_ID = models.ForeignKey(Group, on_delete=models.CASCADE)
    Member_ID = models.ForeignKey(User, on_delete=models.CASCADE)
    Role_ID = models.ForeignKey(Role, on_delete=models.CASCADE)
